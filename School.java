import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class School {
		private String ID;
		private String schName;
		private String schAddress;
		private static final int schTAX = 50;
		static List<School> schList = new ArrayList<>();

		public School() {}
		
		public School(String name, String address) {
			ID = UUID.randomUUID().toString();
			schName = name;
			schAddress = address;
			School.schList.add(this);
		}
		
		public String getID() {
			return ID;
		}

		public void setID(String id) {
			ID = id;
		}

		public String getSchName() {
			return schName;
		}

		public void setSchName(String schName) {
			this.schName = schName;
		}

		public String getSchAddress() {
			return schAddress;
		}

		public void setSchAddress(String schAddress) {
			this.schAddress = schAddress;
		}

		public static int getSchtax() {
			return schTAX;
		}
		
		public static void Message(School sch, String name) {
			String message = "Hello ";
			
			message += name + " and welcome to " + sch.getSchName();


			System.out.println(message);
		}
		
		static public List<Student> GetAllStudentsBySchoolID(String whichID) {
			List<Student> listOfStud = new ArrayList<>();
			
			for(int i = 0; i < Student.studList.size(); i++) {
				if(Student.studList.get(i).getEntityID().equalsIgnoreCase(whichID)) {
					listOfStud.add(Student.studList.get(i));
				}
			}
			
			return listOfStud;
		}
		
		public static int GetIndexOfSchool(String whichID) {
			for(int i = 0; i < School.schList.size(); i++) {
				if(School.schList.get(i).ID.equalsIgnoreCase(whichID)) {
					return i;
				}
			}
			
			return -1;
		}
	}
