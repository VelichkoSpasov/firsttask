import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.List;

public class Manager<E> {
	private Manager<E> asd;
	
	static NumberFormat formatter = new DecimalFormat("#0.00");
	
	public static void PrintFacilityAndAverageGrade(String name, double grade) {
		System.out.println("\nFor facility " + name + " average is: " + grade);
	}
	
	public static void PrintBestPerformer(String name, double grade, String stName) {
		System.out.println("Best performing student for facility " + name + " is " + stName + " is with grade " + grade);
	}
	
	public static void PrintBestPerformerByGender(String name, double grade, String stName, boolean isForMale) {
		if(isForMale)
			System.out.println("Best performing student by Gender male for facility " + name 
					+ " is " + stName 
					+ " is with grade " + grade);
		else {
			System.out.println("Best performing student by Gender female for facility " + name 
					+ " is " + stName 
					+ " is with grade " + grade);
		}
	}	
}
