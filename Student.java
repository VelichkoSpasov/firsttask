import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Student implements Comparable<Student>{
	private String studName;
	private int studAge;
	private String studGender;
	private double studGrade;
	private String entityID;
	private boolean isInSchool = false;
	static List<Student> studList = new ArrayList<>();
	static boolean isMale = false;
	Random rand = new Random();
	
	public Student() {}

	public Student(String name, int age, String gender, String uni_schID, boolean inSchool) {
		studName = name;
		studAge = age;
		studGender = gender;
		studGrade = Double.parseDouble(Manager.formatter.format(2 + (6 - 2) * rand.nextDouble()).replace(',', '.'));
		entityID = uni_schID;
		isInSchool = inSchool;
		Student.studList.add(this);
		
		if(University.GetIndexOfUni(entityID) != -1) {
			University.Message(University.uniList.get(University.GetIndexOfUni(entityID)), name);
		}
		else {
			if(School.GetIndexOfSchool(entityID) != -1) {
				School.Message(School.schList.get(School.GetIndexOfSchool(entityID)), name);
			}
		}
	}
	
	public String toString() {
		String append = "";
		
		if(this.isInSchool)
			append = School.schList.get(School.GetIndexOfSchool(this.entityID)).getSchName();
		else
			append = University.uniList.get(University.GetIndexOfUni(this.entityID)).getUniName();
		
		return "Name: " + this.studName 
				+ " gender: " + this.studGender 
				+ " Grade: " + this.studGrade
				+ " Facility name: " + append;
	}
	
	public String getStudName() {
		return studName;
	}

	public void setStudName(String studName) {
		this.studName = studName;
	}

	public int getStudAge() {
		return studAge;
	}

	public void setStudAge(int studAge) {
		this.studAge = studAge;
	}

	public String getStudGender() {
		return studGender;
	}

	public void setStudGender(String studGender) {
		this.studGender = studGender;
	}

	public double getStudGrade() {
		return studGrade;
	}

	public void setStudGrade(double studGrade) {
		this.studGrade = studGrade;
	}

	public String getEntityID() {
		return entityID;
	}

	public void setEntityID(String entityID) {
		this.entityID = entityID;
	}
	
	public void setIsInSchool(boolean isS_HE) {
		isInSchool = isS_HE;
	}
	
	public boolean getIsInSchool() {
		return isInSchool;
	}
	
	static String isMale() {
		Student.isMale = !Student.isMale;
		
		return Student.isMale ? "male" : "female";
	}
	
	@Override
	public int compareTo(Student arg0) {
		if(this.studGrade == arg0.studGrade)
			return 0;
		else if(this.studGrade < arg0.studGrade)
			return 1;
		else
			return -1;
	}
}
