import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class University {
		private String ID;
		private String uniName;
		private String uniAddress;
		private static final int uniTAX = 100;
		static List<University> uniList = new ArrayList<>();

		public University() {}

		public University(String name, String address) {
			ID = UUID.randomUUID().toString();
			uniName = name;
			uniAddress = address;
			University.uniList.add(this);
		}

		public String getID() {
			return ID;
		}

		public void setID(String id) {
			ID = id;
		}

		public String getUniName() {
			return uniName;
		}

		public void setUniName(String uniName) {
			this.uniName = uniName;
		}

		public String getUniAddress() {
			return uniAddress;
		}

		public void setUniAddress(String uniAddress) {
			this.uniAddress = uniAddress;
		}

		public static int getUnitax() {
			return uniTAX;
		}
		
		public static void Message(University uni, String name) {
			String message = "Hello ";
			
			message += name + " and welcome to " + uni.getUniName();

			System.out.println(message);
		}

		public static int GetIndexOfUni(String whichID) {
			for(int i = 0; i < University.uniList.size(); i++) {
				if(University.uniList.get(i).ID.equalsIgnoreCase(whichID)) {
					return i;
				}
			}
			
			return -1;
		}
	}