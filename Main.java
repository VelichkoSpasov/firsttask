import java.util.ArrayList;
import java.util.Collections;
import java.util.Formatter;
import java.util.List;
import java.util.Random;
import java.util.UUID;

public class Main {
	
	public static void main(String[] args) {
		Random rand = new Random();
		boolean inSchool = false;			
		double income = 0;
		double topContributorTax = 0;
		double tax = 0;
		
		new University("TU-Varna", "Varna");
		new University("Vins", "Varna");
		
		new School("PGMET", "Lovech");
		new School("Vet", "Lovech");
		new School("MG", "Lovech");
		
		Student topContributor = null;
		
		for(int i = 0; i < University.uniList.size(); i++) {
			double average = 0;
			double bestGradeForEntity = 0;
			double bestGradeByGenderM = 0;
			double bestGradeByGenderF = 0;
			int count = 0;
			String studName = null;
			Student male = null;
			Student female = null;
			List<Student> tempListOfStudents = new ArrayList<>();
			
			for(int j = 0; j < 10; j++) {
				Student tempStud = new Student(i + "_Student_" + j, 
						rand.nextInt(20) + 15, 
						Student.isMale(), 
						University.uniList.get(i).getID(), 
						inSchool);
				tempListOfStudents.add(tempStud);
				
				if(bestGradeForEntity < tempStud.getStudGrade()) {
					bestGradeForEntity = tempStud.getStudGrade();
					studName = tempStud.getStudName();
				}
				
				if(tempStud.getStudGender() == "male" && bestGradeByGenderM < tempStud.getStudGrade()) {
					male = tempStud;
					bestGradeByGenderM = male.getStudGrade();
				}
				
				if(tempStud.getStudGender() == "female" && bestGradeByGenderF < tempStud.getStudGrade()) {
					female = tempStud;
					bestGradeByGenderF = female.getStudGrade();
				}
				
				average += tempStud.getStudGrade();
				count++;
			}
			
			String strGrade = Manager.formatter.format(average / count).replace(',', '.');
			double parsedGrade = Double.parseDouble(strGrade);
			
			Manager.PrintFacilityAndAverageGrade(University.uniList.get(i).getUniName(), parsedGrade);
			Manager.PrintBestPerformer(University.uniList.get(i).getUniName(), bestGradeForEntity, studName);
			Manager.PrintBestPerformerByGender(University.uniList.get(i).getUniName(), male.getStudGrade(), male.getStudName(), true);
			Manager.PrintBestPerformerByGender(University.uniList.get(i).getUniName(), female.getStudGrade(), female.getStudName(), false);
			
			for(Student stud : tempListOfStudents) {
				tax = ((stud.getStudAge() / parsedGrade) * 100) + University.getUnitax();
				income += tax;
				
				if(topContributorTax < tax) {
					topContributorTax = tax;
					topContributor = stud;
				}
			}
			
			System.out.println("Income for University " + University.uniList.get(i).getUniName() + ": " 
					+ Double.parseDouble(Manager.formatter.format(income).replace(',', '.')));
			
			System.out.println("Top contributor for this facility is " + topContributor.getStudName() 
				+ " with tax of " + Double.parseDouble(Manager.formatter.format(topContributorTax).replace(',', '.')) + "\n");
			
			income = 0;
			topContributorTax = 0;
			topContributor = null;
		}
		
		
		
		for(int i = 0; i < School.schList.size(); i++) {
			double average = 0;
			double bestGradeForEntity = 0;
			double bestGradeByGenderM = 0;
			double bestGradeByGenderF = 0;
			int count = 0;
			String studName = null;
			Student male = null;
			Student female = null;
			List<Student> tempListOfStudents = new ArrayList<>();
			
			for(int j = 0; j < 10; j++) {
				Student tempStud = new Student(i + "_Student_" + j, 
						rand.nextInt(20) + 15, 
						Student.isMale(), 
						School.schList.get(i).getID(), 
						!inSchool);
				tempListOfStudents.add(tempStud);
				
				if(bestGradeForEntity < tempStud.getStudGrade()) {
					bestGradeForEntity = tempStud.getStudGrade();
					studName = tempStud.getStudName();
				}
				
				if(tempStud.getStudGender() == "male" && bestGradeByGenderM < tempStud.getStudGrade()) {
					male = tempStud;
					bestGradeByGenderM = male.getStudGrade();
				}
				
				if(tempStud.getStudGender() == "female" && bestGradeByGenderF < tempStud.getStudGrade()) {
					female = tempStud;
					bestGradeByGenderF = female.getStudGrade();
				}
			
				average += tempStud.getStudGrade();
				count++;
			}
			
			String strGrade = Manager.formatter.format(average / count).replace(',', '.');
			double parsedGrade = Double.parseDouble(strGrade);
			
			Manager.PrintFacilityAndAverageGrade(School.schList.get(i).getSchName(), parsedGrade);
			Manager.PrintBestPerformer(School.schList.get(i).getSchName(), bestGradeForEntity, studName);
			Manager.PrintBestPerformerByGender(School.schList.get(i).getSchName(), male.getStudGrade(), male.getStudName(), true);
			Manager.PrintBestPerformerByGender(School.schList.get(i).getSchName(), female.getStudGrade(), female.getStudName(), false);
			
			for(Student stud : tempListOfStudents) {
				tax = ((stud.getStudAge() / parsedGrade) * 100) + School.getSchtax();
				income += tax;
				
				if(topContributorTax < tax) {
					topContributorTax = tax;
					topContributor = stud;
				}
			}
			
			System.out.println("Income for School " + School.schList.get(i).getSchName() + ": " 
					+ Double.parseDouble(Manager.formatter.format(income).replace(',', '.')));
			
			System.out.println("Top contributor for this facility is " + topContributor.getStudName() 
				+ " with tax of " + Double.parseDouble(Manager.formatter.format(topContributorTax).replace(',', '.')) + "\n");
			
			income = 0;
			topContributorTax = 0;
			topContributor = null;
		}
	}
}
